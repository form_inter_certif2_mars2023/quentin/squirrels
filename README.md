
<!-- README.md is generated from README.Rmd. Please edit that file -->

# squirrels <img src="man/figures/squirrels_hex.png" align="right" alt="" width="120" />

<!-- badges: start -->
<!-- badges: end -->

The goal of {squirrels} is to analyse data about the squirrels of
Central Park, NYC. Le package a été créé dans le cadre d’une formation
N2

## Installation

You can install the development version of squirrels like so:

``` r
remotes::install_local(path = "~/squirrels_0.0.0.9000.tar.gz")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(squirrels)
get_message_fur_color(primary_fur_color = "Cinnamon")
#> We will focus on Cinnamon squirrels


data_squirrels_reprex <- readr::read_csv(
  system.file(
    "squirrels_reprex_data",
    "nyc_squirrels_sample.csv",
    package = "squirrels"
  )
)
#> New names:
#> • `` -> `...1`
#> Rows: 400 Columns: 37
#> ── Column specification ────────────────────────────────────────────────────────
#> Delimiter: ","
#> chr (14): unique_squirrel_id, hectare, shift, age, primary_fur_color, highli...
#> dbl (10): ...1, long, lat, date, hectare_squirrel_number, zip_codes, communi...
#> lgl (13): running, chasing, climbing, eating, foraging, kuks, quaas, moans, ...
#> 
#> ℹ Use `spec()` to retrieve the full column specification for this data.
#> ℹ Specify the column types or set `show_col_types = FALSE` to quiet this message.

check_squirrel_data_integrity(data_squirrels = data_squirrels_reprex)
#> Everything in the data is ok
```
