# squirrels 0.0.1

## Minor changes

- New function `check_primary_color_is_ok()` to check the data integrity
- New function `get_message_fur_color()` to print a message into the console

# squirrels 0.0.0.9000

* Added a `NEWS.md` file to track changes to the package.
